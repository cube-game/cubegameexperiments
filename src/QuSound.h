#pragma once
#include <string>
#include <map>
#include "ofMain.h"
#include "QuUtilities.h"
#ifdef TARGET_OF_IPHONE
#include "ofxALSoundPlayer.h"
#endif
using namespace std;
class QuSoundStruct
{
	#ifdef TARGET_OF_IPHONE	
	ofxALSoundPlayer mSound; 
	#else 
	ofSoundPlayer mSound; 
	#endif

	QuTimer mVolumeTimer;
	float mMaxVolume;
	float mMinVolume;
	bool loop;
public:
	QuSoundStruct(string filename, bool stream, bool loop, bool multiplay)
	{
		#ifdef TARGET_OF_IPHONE
		if(loop)
			mSound.loadLoopingSound(filename);
		else
			mSound.loadSound(filename);
		#else
		mSound.loadSound(filename, stream);
		mSound.bLoop = loop;
		mSound.setMultiPlay(multiplay);
		#endif

		mMaxVolume = 1;
		mMinVolume = 0;
	}
	~QuSoundStruct()
	{
	}
	void play()
	{
		mSound.play();
	}
	void stop()
	{
		mSound.stop();
	}
	float getVolume()
	{
		float t = mVolumeTimer.getLinear();
		return (mMinVolume*(1-t) + mMaxVolume*t);
	}
	void update()
	{
		float t = mVolumeTimer.getLinear();
		mSound.setVolume(getVolume());
		mVolumeTimer++;
	}
	void setFade(int time, float max)
	{
		if(mVolumeTimer.isSet())
			mMinVolume = getVolume();
		else mMinVolume = 0;
		mMaxVolume = max;
		mVolumeTimer.setTargetAndReset(time);
	}
	bool isFadingOut()
	{
		return mMaxVolume <= mMinVolume;
	}
	#ifdef TARGET_OF_IPHONE
	void setLoop(bool looping){}
	void setMultiplay(bool multiplay){}
	#else
	void setLoop(bool looping)
	{
		mSound.bLoop = looping;
	}
	void setMultiplay(bool multiplay)
	{
		mSound.setMultiPlay(multiplay);
	}
	#endif
};
class QuSoundManager
{
	static QuSoundManager * mSelf;
	map<string,QuSoundStruct *> mSounds;
public:
	static QuSoundManager & getRef()
	{
		//NOTE not thread safe...
		if(mSelf == NULL)
			mSelf = new QuSoundManager();
		return *mSelf;
	}
	~QuSoundManager()
	{
		for(map<string,QuSoundStruct *>::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			delete i->second;
	}
	void loadSound(string filename,bool stream = false, bool loop = false, bool multiplay = false)
	{
		if (mSounds.find(filename) != mSounds.end())
			return;
		mSounds[filename] = new QuSoundStruct(filename,stream,loop,multiplay);
	}
	void playSound(string filename,bool looping = false)
	{
		if (mSounds.find(filename) != mSounds.end())
		{
			mSounds[filename]->play();
			mSounds[filename]->setLoop(looping);
		}
	}
	void stopSound(string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			mSounds[filename]->stop();
	}

	void update()
	{
		for(map<string,QuSoundStruct *>::iterator i = mSounds.begin(); i != mSounds.end(); i++)
			i->second->update();
	}
	void setFade(string filename, bool isFadeIn)
	{
		if (mSounds.find(filename) != mSounds.end())
		{
			if(isFadeIn)
				mSounds[filename]->setFade(20,1);
			else
				mSounds[filename]->setFade(20,0);
		}
	}
	bool isFadingOut(string filename)
	{
		if (mSounds.find(filename) != mSounds.end())
			return mSounds[filename]->isFadingOut();
	}
private:
	QuSoundManager()
	{
	}
};