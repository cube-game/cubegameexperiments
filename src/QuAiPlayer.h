#include "QuUtilities.h"

#include "QuConstants.h"
#include "QuCube.h"
#include <list>
#define MIN(a,b)    ( (a) < (b) ? (a) : (b) )

using namespace std;

class QuAiPlayer
{
	unsigned mC;
	unsigned mPlayer;
	unsigned mDiff;
	QuTimer mHighTimer;
	QuTimer mTurnTimer;
public:
	QuAiPlayer(unsigned aPlayer, unsigned aDiff)
	{
		mDiff = aDiff;
		mPlayer = aPlayer;
		mC = 0;
		mHighTimer.setTargetAndReset(3);
		mTurnTimer.setTargetAndReset(25);
	}
	void update(QuCube & cube)
	{
		//TODO make a thread here to compute strategy

		//if someone has won, we need do nothing
		if(cube.getCurrentPlayer() != mPlayer || cube.getCurMoveType() == 2)
			return;
		
		if(mC == 0)
		{
			//TODO calculate number of moves to look ahead based on number moves made and difficulty
			//cube.getNumberMovesMade();
			int diff = MIN(MAX_AI_DEPTH[cube.getNumberMovesMade()],mDiff);
			cube.calculateAiMove(diff);
			if(mTurnTimer.isExpired())
			{
				mC = 1;
				mTurnTimer.reset();
			}
			else
				mTurnTimer.update();
		}
		else if(mC < 10)
		{
			if(mHighTimer.isExpired())
			{
				while(mC < 10)
				{
					if(cube.highlightMove(FACE_TO_SUBCUBES[cube.getCurrentFace()][mC]))
					{
						mHighTimer.reset();
						mC++;
						break;
					}
					mC++;
				}
			}
			mHighTimer.update();
		}
		else if (mC == 10)
		{
			if(mTurnTimer.isExpired())
			{
				//this looks stupid
				//cube.makeAiHighlight();
				mTurnTimer.reset();
				mC = 11;
			}
			else
			{
				cube.unHighlightCube();
			}
			mTurnTimer.update();
		}
		else if (mC == 11)
		{
			if(mHighTimer.isExpired())
			{
				cube.makeAiMove();
				mHighTimer.reset();
				mC = 12;
			}
			mHighTimer.update();
		}
		else
		{
			if(mTurnTimer.isExpired())
			{
				cube.makeAiTurn();
				mTurnTimer.reset();
				mC = 0;
			}
			mTurnTimer.update();
		}
		
	}
private:
};

class QuPlayerProfile
{
	bool isPlayerAi;
	int mPlayer;
	QuAiPlayer * mAi;
public:
	QuPlayerProfile(int aPlayer)
	{
		mPlayer = aPlayer;
		isPlayerAi = false;
	}
	~QuPlayerProfile()
	{
		if(isPlayerAi)
			delete mAi;
	}
	void reset()
	{
		if(isPlayerAi)
			delete mAi;
		isPlayerAi = false;
	}
	bool isAi()
	{
		return isPlayerAi;
	}
	void createAi(int diff)
	{
		isPlayerAi = true;
		assert(diff >= 0);
		mAi = new QuAiPlayer(mPlayer,diff);
	}
	QuAiPlayer & getAi()
	{
		assert(isPlayerAi);
		return *mAi;
	}
	
};
