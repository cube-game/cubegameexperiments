#pragma once


#include "ofMain.h"
#include "QuCube.h"
#include "QuUtilities.h"
#include "QuCamera.h"
#include "QuDrawer.h"
#include "QuInterface.h"
#include "QuAiPlayer.h"

#ifdef TARGET_OF_IPHONE
#include "ofxiPhone.h"
#include "ofxiPhoneExtras.h"
class testApp : public ofxiPhoneApp{
#else
class testApp : public ofBaseApp{
#endif
private:
	QuCamera * mCam;
	QuCube * mCube;
	QuDrawer * mDraw;
	QuBackground * mBg;
	QuMenu * mMenu;
	QuTouchEffect * mTouchEffect;
	QuOverlayer * mOverlay;
	QuFireworks * mFireworks;

	QuAccelSimulator mAccel;
	QuAccelManager mAccelMan;
	QuMouseDragged mMouse;
	QuBlurrer mBlur;
	QuGameMenu mGameMenu;

	QuPlayerProfile mProf1;
	QuPlayerProfile mProf2;

	QuTimer mGameRestartTimer;

	bool isMenu;
public:
	testApp():mProf1(0),mProf2(1)
	{
	}
	~testApp()
	{
		delete mCam;
		delete mCube;
		delete mDraw;
		delete mBg;
		delete mMenu;
		delete mTouchEffect;
		delete mOverlay;
		delete mFireworks;
	}
	

	void testRoutine();

	void drawMenu();
	void drawGame();
	void updateMenu();
	void updateGame();
	void releaseMenu(int x, int y);
	void releaseGame(int x, int y);
	void pressGame(int x, int y);
	void pressMenu(int x, int y);

	void setup();
	void update();
	void draw();

	void keyPressed  (int key);
	void keyReleased(int key);
	void mouseMoved(int x, int y ){}
	void mouseDragged(int x, int y, int button);
	void mousePressed(int x, int y, int button);
	void mouseReleased(int x, int y, int button);
	void windowResized(int w, int h);

	//iphone stuff
	void iphoneSetup();
	void tiltUpdate();

	#ifdef TARGET_OF_IPHONE
	void touchDown(int x, int y, int id);
	void touchMoved(int x, int y, int id);
	void touchUp(int x, int y, int id);
	void touchDoubleTap(int x, int y, int id);
	
	void lostFocus();
	void gotFocus();
	void gotMemoryWarning();
	void deviceOrientationChanged(int newOrientation);
	#endif

};